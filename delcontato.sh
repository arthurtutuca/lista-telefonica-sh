#!/bin/bash

# n = nome
# s = sobrenome

while getopts ":n:s:" o; do
    case "${o}" in
        n)
            n=${OPTARG}
            ;;
        s)
        	s=${OPTARG}
        	;;
        *)
            echo "argumentos invalidos"
            ;;
    esac
done


qtdadeEncontrados=$(grep "${n} ${s}" contato.txt | wc -l)

if [[ qtdadeEncontrados -gt 1 ]]; then
	echo "seja mais especifico"
else
	grep -v "${n} ${s}" contato.txt >> .temp.txt
	rm contato.txt
	mv .temp.txt contato.txt
fi
