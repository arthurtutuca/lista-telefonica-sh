#!/bin/bash

# n = nome
# s = sobrenome
# d = ddd

while getopts ":n:s:d:" o; do
    case "${o}" in
        n)
            n=${OPTARG}
            ;;
        s)
        	s=${OPTARG}
        	;;
        d)
            d=${OPTARG}
            ;;
        *)
            echo "argumentos invalidos"
            ;;
    esac
done


if [ ! -z "${n}" ]  && [ ! -z "${s}" ] && [ ! -z "${d}" ]; then
    grep "$n\s*$s\s*[(]$d[)]" contato.txt    

elif [ ! -z  "${n}" ] && [ ! -z "${s}" ]; then

    grep "$n\s*$s.*" contato.txt    

elif [ ! -z  "${n}" ]; then
    grep "$n.*" contato.txt   

elif [ ! -z  "${s}" ]; then
    grep ".*\s$s\s.*" contato.txt   

elif [ ! -z  "${d}" ]; then
    grep ".*[()]$d[)].*" contato.txt   

elif [ ! -z  "${s}" ] && [ ! -z "${d}" ]; then
    grep ".*$s[()]$d[)].*" contato.txt   

fi