#!/bin/bash

# n = nome
# s = sobrenome
# d = ddd
# t = telefone

while getopts ":n:s:d:t:" o; do
    case "${o}" in
        n)
            n=${OPTARG}
            ;;
        s)
            s=${OPTARG}
            ;;
        d)
            d=${OPTARG}
            ;;
        t)
        	t=${OPTARG}
        	;;
        *)
            echo "argumentos invalidos"
            ;;
    esac
done

echo "${n} ${s} (${d}) ${t}" >> contato.txt